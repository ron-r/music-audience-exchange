import React from 'react';

export default function() {
  return (
    <div className="footer">
      <span className="rights">Made By Ron Rosenshain &copy; 2018</span>
    </div>
  );
}
