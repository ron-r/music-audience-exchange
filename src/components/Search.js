import React from 'react';

export default function({ artist, handleInputChange, handleSearch }) {
  return (
    <form
      className="search"
      onSubmit={e => {
        e.preventDefault();
        handleSearch();
      }}
    >
      <label>Artist Name</label>
      <input type="text" onChange={handleInputChange} value={artist} />
      <button className="search-btn" type="button" onClick={handleSearch}>
        <i className="fa fa-search" />&nbsp;Search
      </button>
    </form>
  );
}
