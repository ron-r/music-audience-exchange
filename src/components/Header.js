import React from 'react';

export default function() {
  return (
    <div className="header">
      <span className="title">Spotify App</span>
    </div>
  );
}
