import React from 'react';
import NoResults from './NoResults';
import Artist from './Artist';

export default function MatchingResults({ hasResults, handleArtistClick, artists }) {
  return (
    <div className="results">
      <h3>Matching Artists</h3>
      {!hasResults && <NoResults />}

      {hasResults && (
        <ul className="results-list" onClick={handleArtistClick}>
          {artists.map((artist, index) => <Artist key={artist.id} artist={artist} index={index} />)}
        </ul>
      )}
    </div>
  );
}
