import React from 'react';
import NoResults from './NoResults';
import Artist from './Artist';

export default function RealtedArtists({ hasRelated, artists }) {
  return (
    <div>
      {!hasRelated && <NoResults />}

      {hasRelated && (
        <div>
          <h3>Related Artists</h3>
          <ul>
            {artists.map((artist, index) => (
              <Artist key={artist.id} artist={artist} index={index} />
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
