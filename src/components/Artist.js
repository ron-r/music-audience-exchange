import React from 'react';

export default function Artist({ artist, index }) {
  const image =
    artist.images.length > 0 ? (
      <img src={artist.images[artist.images.length - 1].url} alt="Artist" align="middle" />
    ) : (
      <div className="image-placeholder">
        <i className="fa fa-spotify fa-3x" />
      </div>
    );

  return (
    <li key={artist.id} className="artist">
      {image}
      <span data-artist-id={index} className="artist-name">
        {artist.name}
      </span>
      <span className="artist-popularity">{artist.popularity}</span>
    </li>
  );
}
