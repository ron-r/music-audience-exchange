import React from 'react';

export default function() {
  return (
    <div className="loading">
      <i className="fa fa-spinner fa-pulse fa-3x fa-fw" />
      <span className="sr-only">Loading...</span>
    </div>
  );
}
