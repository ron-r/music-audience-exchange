export default function authenticate() {
  // Get the hash of the url
  const hash = window.location.hash
    .substring(1)
    .split('&')
    .reduce(function(initial, item) {
      if (item) {
        var parts = item.split('=');
        initial[parts[0]] = decodeURIComponent(parts[1]);
      }
      return initial;
    }, {});

  window.location.hash = '';

  // Set token
  let token = hash.access_token;

  const authEndpoint = 'https://accounts.spotify.com/authorize';
  const clientId = '2b5e354987f649dd8c94d3dd5602b3d4';
  const scopes = [];
  const redirectUri = window.location.href.split('#')[0];

  // If there is no token, redirect to Spotify authorization
  if (!token) {
    window.location = `${authEndpoint}?client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scopes.join(
      '%20'
    )}&response_type=token`;
  }

  return token;
}
