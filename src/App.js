import React, { Component } from 'react';
import { unionBy } from 'lodash';
import Spotify from 'spotify-web-api-js';

import authenticate from './utils/authenticate';
import './App.css';

import Header from './components/Header';
import Footer from './components/Footer';
import Search from './components/Search';
import MatchingResults from './components/MatchingResults';
import RelatedArtists from './components/RelatedArtists';
import Loading from './components/Loading';

const api = new Spotify();

class App extends Component {
  state = {
    artistInput: '',
    artists: null,
    error: null,
    related: null,
    fetching: false,
    fetchingRelated: false
  };

  componentDidMount() {
    const token = authenticate();
    api.setAccessToken(token);
  }

  handleInputChange = e => {
    this.setState({ artistInput: e.target.value });
  };

  handleSearch = () => {
    this.setState(
      { related: null, fetching: true, artists: null, error: null, fetchingRelated: false },
      () => {
        const { artistInput } = this.state;
        if (!artistInput) {
          alert('Artist name can not be empty');
          return;
        }

        // sanitaize non alphnumeric characters
        const artistName = artistInput.replace(/[^\w\s]/gi, '');

        api.searchArtists(artistName).then(
          data => {
            this.setState({ artists: data.artists.items, fetching: false });
          },
          err => {
            this.setState({ error: err, fetching: false });
          }
        );
      }
    );
  };

  handleArtistClick = e => {
    const artistId = e.target.getAttribute('data-artist-id');

    if (!artistId) {
      return;
    }

    // According to Spotify API page "you can only search one genre at a time"
    // We can take an extra step and use 'OR' if there are 2 genres but that is out of the scope
    // (again, Spotify's API is limiting us and only allowing a single OR in a query)
    const selectedArtist = this.state.artists[artistId];

    this.setState({ fetchingRelated: true, related: null }, () => {
      const promises = selectedArtist.genres.map(genre =>
        api.searchArtists(`genre:"${decodeURIComponent(genre)}"`)
      );

      Promise.all(promises).then(
        data => {
          const related = unionBy(
            data.reduce((artists, item) => artists.concat(item.artists.items), []),
            'id'
          ).filter(artist => artist.id !== selectedArtist.id);

          this.setState({ related, fetchingRelated: false });
        },
        err => {
          this.setState({ error: err, fetchingRelated: false });
        }
      );
    });
  };

  render() {
    const { artistInput, artists, related, fetching, fetchingRelated } = this.state;
    const showResults = artists !== null && !fetching;
    const hasResults = artists && artists.length > 0;

    const showRelated = related !== null;
    const hasRelated = related && related.length > 0;

    return (
      <div className="App">
        <Header />

        <Search
          artist={artistInput}
          handleInputChange={this.handleInputChange}
          handleSearch={this.handleSearch}
        />

        {fetching && <Loading />}

        <div className="content">
          {showResults && (
            <MatchingResults
              hasResults={hasResults}
              handleArtistClick={this.handleArtistClick}
              artists={artists}
            />
          )}

          {fetchingRelated && <Loading />}

          {showRelated && <RelatedArtists artists={related} hasRelated={hasRelated} />}
        </div>

        <Footer />
      </div>
    );
  }
}

export default App;
