# Spotify App  

## Known Issues
* Spotify no longer supports public API so user must login to his spotify account
* Spotify doesn't allow searhing for multiple genres in a single search thus,
 I had to make a search request for each genre

## How To Run
`yarn install`  
`yarn start`